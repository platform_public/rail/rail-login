output "ansible_inventory_v4" {
  value = module.node.inventory_v4
}

output "ansible_inventory_v4_login1" {
  value = module.login_node.inventory_v4
}

output "ansible_inventory_v4_login2" {
  value = module.login_node2.inventory_v4
}

output "ansible_inventory_v4_login3" {
  value = module.login_node3.inventory_v4
}
