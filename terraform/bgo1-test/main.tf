module "node" {
  source = "github.com/TorLdre/tf-nrec-node.git?ref=mods"

  name      = "bgo1_test_rail_admin01"
  node_name = "admin-01"
  # Enable this to create and update fqdn in NREC
  zone_name         = "bgo1.test.rail.uhdc.no"
  domain            = "bgo1.test.rail.uhdc.no"
  region            = "bgo"
  node_count        = 1
  ssh_public_key    = "~/.ssh/id_rsa.pub"
  allow_ssh_from_v6 = ["2001:700:200::/48"]
  allow_ssh_from_v4 = ["129.177.0.0/16"]
  network           = "dualStack"
  flavor            = "m1.large"
  image_name        = "GOLD Alma Linux 9"
  image_user        = "almalinux"
  volume_size       = 0
}

module "login_node" {
  source = "github.com/TorLdre/tf-nrec-node.git?ref=mods"

  name      = "bgo1_test_rail_login1"
  node_name = "login-01"
  # Enable this to create and update fqdn in NREC
  zone_name         = "bgo1.test.rail.uhdc.no"
  domain            = "bgo1.test.rail.uhdc.no"
  region            = "bgo"
  node_count        = 1
  ssh_public_key    = "~/.ssh/id_rsa.pub"
  allow_ssh_from_v6 = ["::/0"]
  allow_ssh_from_v4 = ["0.0.0.0/0"]
  network           = "dualStack"
  flavor            = "d1.medium"
  image_name        = "GOLD Alma Linux 9"
  image_user        = "almalinux"
  volume_size       = 0
}

module "login_node2" {
  source = "github.com/TorLdre/tf-nrec-node.git?ref=mods"

  name      = "bgo1_test_rail_login2"
  node_name = "login-02"
  # Enable this to create and update fqdn in NREC
  zone_name         = "bgo1.test.rail.uhdc.no"
  domain            = "bgo1.test.rail.uhdc.no"
  region            = "bgo"
  node_count        = 1
  ssh_public_key    = "~/.ssh/id_rsa.pub"
  allow_ssh_from_v6 = ["::/0"]
  allow_ssh_from_v4 = ["0.0.0.0/0"]
  network           = "dualStack"
  flavor            = "m1.small"
  image_name        = "GOLD Alma Linux 9"
  image_user        = "almalinux"
  volume_size       = 0
}

module "login_node3" {
  source = "git@github.com:TorLdre/tf-nrec-node.git?ref=mods"

  name      = "bgo1_test_rail_login3"
  node_name = "login-03"
  # Enable this to create and update fqdn in NREC
  zone_name         = "bgo1.test.rail.uhdc.no"
  domain            = "bgo1.test.rail.uhdc.no"
  region            = "bgo"
  node_count        = 1
  ssh_public_key    = "~/.ssh/id_rsa.pub"
  allow_ssh_from_v6 = ["::/0"]
  allow_ssh_from_v4 = ["0.0.0.0/0"]
  network           = "dualStack"
  flavor            = "shpc.c1a.8xlarge"
  image_name        = "GOLD Alma Linux 9"
  image_user        = "almalinux"
  volume_size       = 1000
  volume_type       = "mass-storage-ssd"
}

