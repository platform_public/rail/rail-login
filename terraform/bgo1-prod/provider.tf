# Shared state
#provider "openstack" {}

terraform {
  backend "s3" {
    endpoints                   = { s3 = "https://object.api.bgo.nrec.no:8080" }
    bucket                      = "admin-terraform-state"
    use_path_style              = true
    key                         = "bgo1-admin-01.tfstate.tf"
    region                      = "bgo"
    skip_credentials_validation = true
    skip_region_validation      = true
    skip_requesting_account_id  = true
    skip_s3_checksum            = true
  }
}
