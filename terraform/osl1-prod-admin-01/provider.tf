# Shared state
#provider "openstack" {}

terraform {
  backend "s3" {
    endpoints                   = { s3 = "https://object.api.osl.nrec.no:8080" }
    bucket                      = "login-terraform-state"
    use_path_style              = true
    key                         = "osl1-admin-01.tfstate.tf"
    region                      = "osl"
    skip_credentials_validation = true
    skip_region_validation      = true
    skip_requesting_account_id  = true
    skip_s3_checksum            = true
  }
}
