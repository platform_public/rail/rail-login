module "node" {
  source = "git@github.com:TorLdre/tf-nrec-node.git?ref=mods"

  name      = "osl1_prod_rail_admin1"
  node_name = "osl1-prod-admin-01"
  # Enable this to create and update fqdn in NREC
  zone_name         = "osl1.prod.rail.uhdc.no"
  domain            = "osl1.prod.rail.uhdc.no"
  region            = "osl"
  node_count        = 1
  ssh_public_key    = "~/.ssh/id_rsa.pub"
  allow_ssh_from_v6 = ["2001:700:200::/48"]
  allow_ssh_from_v4 = ["129.177.0.0/16"]
  network           = "dualStack"
  flavor            = "m1.large"
  image_name        = "GOLD Alma Linux 9"
  image_user        = "almalinux"
  volume_size       = 0
}
