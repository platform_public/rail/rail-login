output "ansible_inventory_v4" {
  value = module.node.inventory_v4
}

output "ansible_inventory_v4_login1" {
  value = module.login_node.inventory_v4
}
