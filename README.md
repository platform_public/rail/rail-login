# Mgmt repo for RAIL login and admin nodes

## Terraform (optional)

Use terraform to create a new RAIL login node with Alma linux

``` bash
cd terraform
terraform init
terraform apply
terraform output -raw ansible_inventory_v4 > ../inventory
```

The last line create a new inventory file for ansible

## Ansible

This should work with Almalinux rail login and admin nodes.

First install the needed roles:

``` bash
ansible-galaxy install -r requirements.yaml
```

All config is in `group_vars/all.yaml` except Openstack Cli installation
and a few other distro packages in `vars/<distro>.yaml`

### Run ansible

Make sure `rail_login` and `rail_admin` groups are defined `inventory` and that the rail nodes are
in the group. To run a full profile setup:

``` bash
ansible-playbook profile/login.yaml 
```

To run only common (new users, packages, repo, etc) run:

``` bash
ansible-playbook profile/login.yaml --skip-tags sshd,redis
```

